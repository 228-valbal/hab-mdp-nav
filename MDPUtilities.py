import collections, random

############################################################
# An algorithm that solves an MDP (i.e., computes the optimal
# policy).
class MDPAlgorithm:
    # Set:
    # - self.pi: optimal policy (mapping from state to action)
    # - self.V: values (mapping from state to best values)
    def solve(self, mdp): raise NotImplementedError("Override me")

# An abstract class representing a Markov Decision Process (MDP).
class MDP:
    # Return the start state.
    def startState(self): raise NotImplementedError("Override me")

    # Return set of actions possible from |state|.
    def actions(self, state=None): raise NotImplementedError("Override me")

    # Return a list of (newState, prob, reward) tuples corresponding to edges
    # coming out of |state|.
    # Mapping to notation from class:
    #   state = s, action = a, newState = s', prob = T(s, a, s'), reward = Reward(s, a, s')
    # If IsEnd(state), return the empty list.
    def succAndProbReward(self, state, action): raise NotImplementedError("Override me")

    def discount(self): raise NotImplementedError("Override me")

    # Compute set of states reachable from startState.  Helper function for
    # MDPAlgorithms to know which states to compute values and policies for.
    # This function sets |self.states| to be the set of all states.
    def computeStates(self):
        self.states = set()
        queue = []
        self.states.add(self.startState())
        queue.append(self.startState())
        while len(queue) > 0:
            state = queue.pop()
            for action in self.actions(state):
                for newState, prob, reward in self.succAndProbReward(state, action):
                    if newState not in self.states:
                        self.states.add(newState)
                        queue.append(newState)


############################################################
# Custom MDP for our ground side controller
#
class CustomizableMDP(MDP):
    def __init__(self, observedQueryState, actions, min_ballast, min_height, optimal_height, height_bias):
        """
        observedQueryState: current vehicle state from most recent downlink data
        actions: list of possible actions
        min_ballast: lowest allowable ballast amount
        min_height: lower bound on altitude
        optimal_height: our optimal height for this stage of flight
        height_bias: how much our controller should care about maintaining current height
        """
        self.observedQueryState = observedQueryState
        self.actions_list = actions
        self.min_ballast = min_ballast
        self.min_height = min_height
        self.optimal_height = optimal_height
        self.height_bias = height_bias

    # Return the start state.
    def startState(self):
        return self.observedQueryState

    # Return set of actions possible from |state|. Note we assume all actions are possible at all states of flight
    def actions(self):
        return self.actions_list


