import math, random, time
from numpy.random import normal
from collections import defaultdict
from operator import itemgetter
from MDPUtilities import *
from flightSimulator import Sim
import matplotlib.pyplot as plt

## NOTE THAT STATE = (HEIGHT, H', REM_BALLAST, TIME)

############################################################
class ValueIteration(MDPAlgorithm):
    '''
    Solve the MDP using value iteration.  solve() sets
    - self.V to the dictionary mapping states to optimal values
    - self.pi to the dictionary mapping states to an optimal action
    Note: epsilon is the error tolerance.
    The ValueIteration class is a subclass of MDPUtilities.MDPAlgorithm
    '''
    def solve(self, mdp, epsilon=0.001):
        mdp.computeStates()
        def computeQ(mdp, V, state, action):
            # Return Q(state, action) based on V(state).
            return sum(prob * (reward + mdp.discount() * V[newState]) \
                            for newState, prob, reward in mdp.succAndProbReward(state, action))

        def computeOptimalPolicy(mdp, V):
            # Return the optimal policy given the values V.
            pi = {}
            for state in mdp.states:
                pi[state] = max((computeQ(mdp, V, state, action), action) for action in mdp.actions(state))[1]
            return pi

        V = collections.defaultdict(float)  # state -> value of state
        numIters = 0
        while True:
            newV = {}
            for state in mdp.states:
                newV[state] = max(computeQ(mdp, V, state, action) for action in mdp.actions(state))
            numIters += 1
            if max(abs(V[state] - newV[state]) for state in mdp.states) < epsilon:
                V = newV
                break
            V = newV

        # Compute the optimal policy now
        pi = computeOptimalPolicy(mdp, V)
        print "ValueIteration: %d iterations" % numIters
        self.pi = pi
        self.V = V


############################################################
class MCTS(MDPAlgorithm):
    """ Main ground side policy solver
    """
    def get_reward(self, state, action, mdp):
        wrongWayPenalty = 0
        actionPenalty = 0
        outOfBallastPenalty = 0
        velocityControlPenalty = 0
        distancePenalty = 0
        if (state[1] > 0 and "ballast" in action) or (state[1] < 0 and "vent" in action):
            wrongWayPenalty = -50
        if "ballast" in action or "vent" in action:
            actionPenalty = -20
        if state[2] < mdp.min_ballast:
            outOfBallastPenalty = -50
        if abs(state[1]) > 0:
            velocityControlPenalty = -.05 * abs(state[1])
        if abs(state[0] - mdp.optimal_height) > 1000:
            distancePenalty = -mdp.height_bias*abs(state[0] - mdp.optimal_height)**2

        # tuned reward 
        return distancePenalty + velocityControlPenalty + outOfBallastPenalty + actionPenalty + wrongWayPenalty
        # original reward
        # return -(state[2] < mdp.min_ballast) - (state[0] - mdp.min_height) - .3*abs(state[1])- mdp.height_bias*(state[0] - mdp.optimal_height)**2


    def generatorFunction(self, state, action, mdp):
        new_state = None
        if action == "vent_5":
            vf = state[1] + normal(-0.107574051735302, 0.0469317579370726)
            delta_h = (state[1] + vf)/2.0 * 5.0
            new_state = (state[0] + delta_h, vf, state[2], state[3]+5)
        if action == "ballast_5":
            vf = state[1] + normal(0.127355555555556, 0.0291666666666667)
            delta_h = (state[1] + vf)/2.0 * 5.0
            new_state = (state[0] + delta_h, vf, state[2]-.00415, state[3]+5)
        if action == "nothing_60":
            vf = state[1] + normal(0, 0.08)
            delta_h = (state[1] + vf)/2.0 * 60.0
            new_state = (state[0] + delta_h, state[1], state[2], state[3]+60)
        return new_state, self.get_reward(new_state, action, mdp)

    def getCached(self, state, Q):
        """ A bit of a hacky way to get a nearest neighbor cached Q value if we need to cut time in a
            real flight scenario
        """
        similar_states = [(state[0]+5*i, state[1]+5*i, state[2]+.2*i, state[3]+10*i) for i in [-5,-4,-3,-2,-1,1,2,3,4,5]].append(state)
        return max((key for key in Q if key[0] in similar_states), key=itemgetter(1))

    def solve(self, mdp, depth=210, max_simulations=200, future_discount = .9):
        Q = collections.defaultdict(float)
        N = collections.defaultdict(float)
        T = []
        actions = mdp.actions()
        for _ in range(max_simulations):
            self.simulate(mdp.startState(), depth, actions, Q, N, T, future_discount, mdp)
        return Q, T

    def rollout(self, state, depth, actions, future_discount, mdp):
        if depth == 0:
            return 0

        # a slightly smarter rollout that wont let us break through a bound without a fight
        a = "nothing_60"
        if abs(state[0] - 14000) < 200 and state[1] > 0:
            a = "vent_5"
        elif abs(state[0] - 12000) < 200 and state[1] < 0:
            a = "ballast_5"

        next_state, reward = self.generatorFunction(state, a, mdp)
        return reward + future_discount*self.rollout(next_state, depth - 1, actions, future_discount, mdp)

    def simulate(self, state, depth, actions, Q, N, T, future_discount, mdp):
        if depth == 0:
            return 0
        if state not in T:
            for action in actions:
                N[(state, action)] = 0
                Q[(state, action)] = 0
            T.append(state)
            return self.rollout(state, depth, actions, future_discount, mdp)
        max_val = max([Q[(state, key)] for key in actions]) # + optionally add the exploration bonus
        a = actions[0]
        for action in actions:
            if Q[(state, action)] == max_val:
                a = action
                break
        next_state, reward = self.generatorFunction(state, a, mdp)
        q = reward + self.simulate(next_state, depth - 1, actions, Q, N, T, future_discount, mdp)
        N[(state, a)] += 1
        Q[(state, a)] += (q - Q[(state, a)])/N[(state, a)]
        return q

    def computeOptimalPolicy(self, states, Q):
        # Return the optimal policy given the values V.
        pi = {}
        for state in states:
            # optionally generalize here
            pi[state] = self.getCached(state, Q)
        return pi


def getBestAction(Q, state):
    max_val = max([Q[(current_state, key)] for key in actions]) # + optionally add the exploration bonus
    a = actions[0]
    for action in actions:
        if Q[(current_state, action)] == max_val:
            a = action
            break


############################################################
# evaluate MCTS with the balloon sim
############################################################
def RunEvaluationOnSimulator():
    simulator = Sim()

    actions = ["vent_5", "ballast_5", "nothing_60"]
    min_ballast = 0
    min_height = 10000 #meters
    optimal_height = 13000 #meters
    height_bias = .001
    current_state = simulator.envReset()
    current_state = (current_state[0], current_state[1], current_state[2], 0)
    currentMDP = CustomizableMDP(current_state, actions, min_ballast, min_height, optimal_height, height_bias)
    MCTS_solver = MCTS()
    Q, states = MCTS_solver.solve(currentMDP)

    hs = []
    ts = []
    num_actions = 0
    try:
      for _ in range(1000):
        max_val = max([Q[(current_state, key)] for key in actions]) # + optionally add the exploration bonus
        a = actions[0]
        for action in actions:
            if Q[(current_state, action)] == max_val:
                a = action
                break
        print current_state[0]
        print a
        best_action = 3
        sim_steps = 60/5
        if a == "vent_5":
            best_action = 1
            plt.axvline(x=simulator.t/3600., color='blue')
            sim_steps = 1
            num_actions += 1
        elif a == "ballast_5":
            best_action = 2
            plt.axvline(x=simulator.t/3600., color='black')
            sim_steps = 1
            num_actions += 1
        for _ in range(sim_steps):
            current_state = simulator.envStep(best_action)
        current_state = simulator.state()
        hs.append(simulator.h)
        ts.append(simulator.t/3600.)
        current_state = (current_state[0], current_state[1], current_state[2], simulator.t)
        currentMDP.observedQueryState = current_state
        Q, states = MCTS_solver.solve(currentMDP)
    except KeyboardInterrupt: pass
    plt.plot(ts,hs)
    plt.show()
    print "NUM ACTIONS"
    print num_actions
    # end of flight! 


if __name__ == "__main__":
    RunExample()



